# Zadanie rekrutacyjne

## Opis
Zadaniem polega stworzeniu części backendowej odpowiedzialnej za dostarczenie API do pobierania postów oraz komentarzy 
do nich oraz części frontendowej odpowiedzialnej za pobierania i wyświetlanie danych.

### Backend 
W folderze src znajdują się przykładowe klasy obrazujące wycinek rzeczywistości 
(posty powiązane w relacji jeden do wielu z komentarzami). Należy stworzyć API w oparciu o architekturę **REST** oraz relacyjną
bazę danych **MySql**. Preferowany framework **Symfony** oraz **ORM Doctrine**. Dobór pozostałych bibliotek w kwesti uznania.

Wymagana endpointy (powinny zwracać dane w formacie json):

| URI | METODA | OPIS |
| ------ | ------ | ------ |
| /posts | GET | zwracająca wszystkie posty |
| /posts | POST | tworzące encję Post |
|/posts/__{postId}__/comments | GET | pobierającą wszystkie komentarza encji POST o id równym parametrowi postId |


### Frontend

Aplikacja powinna połączyć się z częścią backendową i stworzyć komponenty:
* listę wszystkich postów
* formularz dodawanie nowych postów
* po wybraniu jednego z postów (z rozwijanego pola select, bądź dodanie akcji w każdym wierszu na liście wszystkich postów),
wyświetlanie wszystkich komentarzy danego postu.

Aplikacja nie musi zawierać w sobie routingu.
Preferowana biblioteka **React**.  Dobór pozostałych bibliotek jak i frameworków cssowych w kwesti uznania. 
