<?php


class Post
{

    /**
     * @var int
     */
    private $id;
    /**
     * @var \DateTime
     */
    private $cratedAt;

    /**
     * @var string
     */
    private $text;

    /**
     * @var array
     */
    private $comments;

    /**
     * Post constructor.
     * @param DateTime $cratedAt
     * @param string $text
     * @param array $comments
     */
    public function __construct(DateTime $cratedAt, $text, array $comments)
    {
        $this->cratedAt = $cratedAt;
        $this->text = $text;
        $this->comments = $comments;
    }

    /**
     * @return DateTime
     */
    public function getCratedAt()
    {
        return $this->cratedAt;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return array
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function addComment(Comment $comment){
        $this->comments[]=$comment;
    }
}