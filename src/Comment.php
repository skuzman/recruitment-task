<?php
/**
 * Created by PhpStorm.
 * User: skuzman
 * Date: 22.02.18
 * Time: 18:09
 */

class Comment
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $author;

    /**
     * @var Post
     */
    private $post;

    /**
     * Comment constructor.
     * @param DateTime $createdAt
     * @param string $text
     * @param string $author
     * @param Post $post
     */
    public function __construct(DateTime $createdAt, $text, $author, Post $post)
    {
        $this->createdAt = $createdAt;
        $this->text = $text;
        $this->author = $author;
        $this->post = $post;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

}